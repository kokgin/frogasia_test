<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'PondController@index');

Route::get('frogs/add/{gender}', 'FrogController@add');
Route::get('frogs/goodbye/{id}', 'FrogController@goodbye');

Route::get('plants/create', 'PlantController@create');
Route::get('plants/remove/{id}', 'PlantController@remove');