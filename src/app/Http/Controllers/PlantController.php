<?php namespace App\Http\Controllers;

use App\Plant;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class PlantController extends Controller {

    public function create()
    {
        $plant = Plant::create([]);

        return redirect('/')->with('message', 'Plant #' . $plant->id . ' added');
    }

    public function remove($id)
    {
        $plant = Plant::find($id);

        if (is_null($plant))
        {
            return redirect('/')->with('warning', 'Invalid Plant ID');
        }

        $plant->delete($id);

        return redirect('/')->with('message', 'Plant #' . $id . ' removed');
    }

}
