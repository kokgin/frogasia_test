<?php namespace App\Http\Controllers;

use App\Frog;
use App\Plant;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class PondController extends Controller {

    public function index()
    {
        $plants = Plant::all();
        $frogs = Frog::all();

        return view('pond', compact('frogs', 'plants'));
    }

}
