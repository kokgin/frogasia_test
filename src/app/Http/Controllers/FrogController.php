<?php namespace App\Http\Controllers;

use App\Frog;
use App\Plant;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use Faker\Factory as Faker;

class FrogController extends Controller {

    public function add($gender)
    {
        # Check if pond can afford more frogs
        if (!$this->population())
        {
            return redirect('/')->with('warning', 'Add more plant to support frog population');
        }

        # Add new frog in
        $faker = Faker::create();
        $frog = new Frog;

        if ($gender == 'M')
        {
            $frog->name = $faker->firstNameMale;
        }
        else
        {
            $frog->name = $faker->firstNameFemale;
        }

        $frog->gender = $gender;
        $frog->save();

        return redirect('/')->with('message', 'Welcome ' . $frog->name);
    }

    # This function check between plants and frogs
    protected function population()
    {
        $plantQty = Plant::count();
        $frogQty = Frog::count();

        # Here, we assume 1 plant can afford 5 frogs
        if ($plantQty * 5 <= $frogQty)
        {
            return false;
        }

        return true;
    }

    public function goodbye($id)
    {
        $frog = Frog::find($id);

        if (is_null($frog))
        {
            return redirect('/')->with('warning', 'Invalid Frog ID');
        }

        $name = $frog->name;

        $frog->delete($id);

        return redirect('/')->with('message', 'Goodbye ' . $name);
    }

}
