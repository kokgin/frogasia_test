<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFrogsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('frogs', function(Blueprint $table)
		{
			$table->increments('id');
            $table->string('name', 50);
            $table->enum('gender', ['M', 'F']);
            # Phases: E=Egg, T=Tadpole, TL=Tadpole with Legs, YF=Young Frog, F=Adult Frog
            $table->enum('phase', ['E', 'T', 'TL', 'YF', 'AF']);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('frogs');
	}

}
