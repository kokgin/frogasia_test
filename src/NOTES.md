# NOTES
* This project use Laravel 5 PHP Framework
* It use Composer to manage dependencies
* It use Faker to generate name for male and female frogs
* The coding part took about 3 hours to complete

# SPECIAL INSTRUCTIONS
* Install composer if not yet install. Please refer to https://getcomposer.org/doc/00-intro.md#installation-linux-unix-osx
* Go to __src__ folder, run command: ```composer install``` to install all required packages
* Point website root folder to __src\public__
* Make sure __src\storage__ folder is writable by web server: ```sudo chmod -R 777 storage```
* If your server is running _SELinux_ with enforcing mode, run this command to allow __src\storage__ folder to be writable by web server: ```chcon -R -t httpd_sys_rw_content_t storage/```
* Make sure the website root folder is able to run __.htaccess__ file. Example of _VirtualHost_ setting in _Apache_:

		<VirtualHost *:80>
			ServerName tyler.test
			DocumentRoot /var/www/test/public
			DirectoryIndex index.php
			<Directory /var/www/test/public/>
				Options Indexes FollowSymLinks
				AllowOverride All
				Require all granted
			</Directory>
		</VirtualHost>

* This system needs a database, by default it supports _SQLite_, _MySQL_, _Postgres_ and _SQL Server_
* Rename __src/.env.example__ to __src/.env__, then configure database settings (_DB\_HOST_, _DB\_DATABASE_, _DB\_USERNAME_, & _DB\_PASSWORD_)
* Inside __src__ folder, run command: ```php artisan migrate``` to auto generate tables for your selected database
* Done

# TROUBLESHOOTING
If you see a blank page when try to access the system via web browser, normally it is caused by:

* __src\storage__ write permission denied (either chmod or chcon permission)
* your database issues (such as connection failed or table not exists)
* __src\public\.htaccess__ file is not runnable due to __VirtualHost__ setting (Laravel need it to support _pretty-URL_)

# THOUGHTS: WHY USE Laravel Framework?
* It is well structured
* It takes care many things for us
* It is fun and powerful
* No matter big or small project, using framework shall be a good way to go