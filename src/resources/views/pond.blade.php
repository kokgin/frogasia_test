<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Frog Pond</title>

    <link href="{{ asset('/css/app.css') }}" rel="stylesheet">

    <!-- Fonts -->
    <link href='//fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <style>
        .btn { margin: 2px; }
    </style>
</head>
<body>

<div class="container">
    <br>

    @if (Session::has('message'))
        <div class="alert alert-success">
            <a href="#" class="close" data-dismiss="alert">&times;</a>
            {{ Session::get('message') }}
        </div>
    @endif

    @if (Session::has('warning'))
        <div class="alert alert-warning">
            <a href="#" class="close" data-dismiss="alert">&times;</a>
            {{ Session::get('warning') }}
        </div>
    @endif

    <div class="row">
        <div class="col-xs-12 col-xs-offset-0">
            <div class="panel panel-default">
                <div class="panel-body">
                    <a class="btn btn-success" href="{{ url('/plants/create') }}" role="button">Add plant</a>
                    <a class="btn btn-primary" href="{{ url('/frogs/add/M') }}" role="button">Add male frog</a>
                    <a class="btn btn-danger" href="{{ url('/frogs/add/F') }}" role="button">Add female frog</a>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12 col-xs-offset-0">
            <div class="panel panel-default" style="background-color: #5bc0de">
                <div class="panel-body">
                    @foreach ($plants as $plant)
                        <a class="btn btn-success" href="{{ url('/plants/remove/' . $plant->id) }}" role="button" title="click to remove">Plant #{{ $plant->id }}</a>
                    @endforeach

                    @foreach ($frogs as $frog)
                        <a class="btn {{ $frog->gender == 'M' ? 'btn-primary' : 'btn-danger' }}" href="{{ url('/frogs/goodbye/' . $frog->id) }}" role="button" title="click to bid farewell">{{ $frog->name }}</a>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Scripts -->
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.1/js/bootstrap.min.js"></script>
</body>
</html>
